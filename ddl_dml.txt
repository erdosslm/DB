CREATE TABLE students
(
    stu_id SERIAL PRIMARY KEY,
    stu_name VARCHAR(30),
    stu_phone VARCHAR(30)
);

SELECT * FROM students;

INSERT INTO students(stu_name,stu_phone)
VALUES ('Anel','729211'),
('Ilyas ','729211'),
('Timur ','729211');


CREATE TABLE Group_s
(
    g_id SERIAL PRIMARY KEY,
    g_name VARCHAR(30)
);

SELECT * FROM Group_s;

INSERT INTO Group_s(g_name)
VALUES ('cs-1901'),
('cs-1902'),
('cs-1903'),
('cs-1904'),
('cs-1905');